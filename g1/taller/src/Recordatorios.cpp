#include <iostream>
#include <list>

using namespace std;

using uint = unsigned int;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10
//
// Ejercicio 7


// Clase Fecha
class Fecha {
  public:
    // Completar declaraciones funciones
	int mes();
	int dia();
	Fecha(int mes, int dia);
	void incrementar_dia();
    bool operator==(Fecha o);

  private:
	int mes_;
	int dia_;
    //Completar miembros internos
};

int Fecha::mes(){
	return mes_;

}
int Fecha::dia(){
	return dia_;
}

Fecha::Fecha(int mes, int dia) : mes_(mes), dia_(dia) {
}

//Ejericio 8

ostream& operator<<(ostream& os, Fecha f){
	os << f.dia() << '/' << f.mes();
	return os;
}

bool Fecha::operator==(Fecha o) {
    bool igualDia = this->dia() == o.dia();
    bool igualMes = this->mes() == o.mes();
    return igualMes && igualDia;
}

//ejercicio 10

void Fecha::incrementar_dia(){
	if (dia_ < dias_en_mes(mes_)){
		dia_++;
	}
	else{
		if (mes_<12){
		mes_++;
		}
		else{
			mes_ = 1;
		}
		dia_ = 1;
	    }
}

// Ejercicio 11, 12

class Horario {
  public:
	int hora();
	int min();
	Horario(uint hora, uint min);

	bool operator<(Horario h);

  private:
	int hora_;
	int min_;
};

int Horario::hora(){
	return hora_;

}
int Horario::min(){
	return min_;
}

Horario::Horario(uint hora, uint min) : hora_(hora), min_(min) {
}

ostream& operator<<(ostream& os, Horario h){
	os << h.hora() << ':' << h.min();
	return os;
}

//Ejercicio 12

bool Horario::operator<(Horario h) {
	if(hora_ < h.hora()){
		return true;
	}
	else if(hora_ == h.hora()){
			return min_ < h.min();
		}
	return false;
}

// Ejercicio 13

class Recordatorio {
  public:
    string mensaje();
    Fecha fecha();
    Horario horario();
	Recordatorio(Fecha f, Horario h, string msj);
	bool operator<(Recordatorio r);

  private:
	string msj_;
	Fecha f_;
	Horario h_;
};

ostream& operator<<(ostream& os, Recordatorio r){
	os << r.mensaje() << " @ " << r.fecha() << " " << r.horario();
	return os;
}

string Recordatorio::mensaje() {
	return msj_;
}
Fecha Recordatorio::fecha() {
	return f_;
}
Horario Recordatorio::horario() {
	return h_;
}

Recordatorio::Recordatorio(Fecha f, Horario h, string msj) : f_(f), h_(h),msj_(msj) {
}

bool Recordatorio::operator<(Recordatorio r) {
	return h_<r.horario();
}

// Ejercicio 14

class Agenda {
public:
Agenda(Fecha f0);
void agregar_recordatorio(Recordatorio rec);
void incrementar_dia();
list<Recordatorio> recordatorios_de_hoy();
Fecha hoy();


private:
Fecha f_;
list<Recordatorio> listRec_;
};

Agenda::Agenda(Fecha f0) : f_(f0) {
}

void Agenda::incrementar_dia(){
	f_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec){
	listRec_.push_back(rec);
}

Fecha Agenda::hoy(){
	return f_;
}


list<Recordatorio> Agenda::recordatorios_de_hoy(){
	list<Recordatorio> recordatorios;
	for(Recordatorio i:listRec_){
		if (i.fecha() == f_){
			recordatorios.push_back(i);
		}
	}
	recordatorios.sort();
	return recordatorios;
}

ostream& operator<<(ostream& os, Agenda r) {
    os << r.hoy() << endl
       << "=====" << endl;
    for (Recordatorio rec: r.recordatorios_de_hoy()) {
        os << rec << endl;
    }
    return os;
}

