#include "conjunto.h"


template <class T>
Conjunto<T>::Nodo::Nodo(const T& v) {
    valor = v;
    izq = nullptr;
    der = nullptr;
    padre = nullptr;
}

template <class T>
Conjunto<T>::Conjunto() : _size(0), _raiz(nullptr){
}

template <class T>
Conjunto<T>::~Conjunto() { 
    while(_size > 0){
        remover(maximo());
    }
    // delete this;
}

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {

    Nodo* x = this->_raiz;

    while(x!=nullptr){
        if (x->valor == clave){
            return true;
        } else if (x->valor < clave){

            x = x->der;
        } else if (x->valor > clave){

            x = x->izq;
        }
    }
    return false;
}

template <class T>
void Conjunto<T>::insertar(const T& clave) {
    // si ya pertenece no hago nada, chequear si pertenece es O(lg n)
    if(!pertenece(clave)){ // si la clave no esta opero
        Nodo* padre = nullptr;
        Nodo* hijo = this -> _raiz;
        while(hijo != nullptr){ // ciclo para bajar en el arbol

            padre = hijo;
            if(clave < hijo -> valor){
                hijo = hijo -> izq;
            }
            else {
                hijo = hijo -> der;
            }
        }

        if(padre == nullptr){
            this -> _raiz = new Nodo(clave); // el arbol estaba vacio
        } else if (clave < padre -> valor){
            padre -> izq = new Nodo(clave);
            padre -> izq -> padre = padre;
        } else{
            padre -> der = new Nodo(clave);
            padre -> der -> padre = padre;
        }
    _size++;
    }



}





template <class T>
void Conjunto<T>::transplantar(Nodo* aBorrar, Nodo* aTransplantar) {
    if(aBorrar -> padre == nullptr){
        this -> _raiz = aTransplantar;
    } else if(aBorrar == aBorrar -> padre -> izq){ // soy hijo izquierdo
        aBorrar -> padre -> izq = aTransplantar;
    } else{                                        // soy hijo derecho
        aBorrar -> padre -> der = aTransplantar;
    }
    if(aTransplantar != nullptr){
        aTransplantar -> padre = aBorrar -> padre;
    }
}

template <class T>
 void Conjunto<T>::remover(const T& clave) {
    if(pertenece(clave)){
        Nodo* aBorrar = nullptr;
        Nodo* y;
        Nodo* x = this->_raiz;

        while(aBorrar != x && x!=nullptr){ // Ciclo para bajar en el arbol
            if (x->valor == clave){
                aBorrar = x;
            } else if (x->valor < clave){
                y = x;
                x = x->der;
            } else if (x->valor > clave){
                y = x;
                x = x->izq;
            }
        }
        // aBorrar = aBorrar -> padre

        if(aBorrar -> izq == nullptr){          //Caso facil 1
            transplantar(aBorrar, aBorrar -> der);
        } else if(aBorrar -> der == nullptr){   //Caso facil 2
            transplantar(aBorrar, aBorrar -> izq);
        } else{                                 //Caso dificil
            Nodo* sucesor = aBorrar -> der;     //Voy a buscar el sucesor
            while(sucesor -> izq !=nullptr){
                sucesor = sucesor -> izq;
            }
            if(sucesor -> padre != aBorrar){    //Soluciono los problemas de quitar al sucesor de su posicion
                transplantar(sucesor, sucesor -> der);
                sucesor -> der = aBorrar -> der;
                sucesor -> der -> padre = sucesor;
            }
            transplantar(aBorrar, sucesor); //Transplanto quitando aBorrar
            sucesor -> izq = aBorrar -> izq;
            sucesor -> izq -> padre = sucesor;
        }
        delete aBorrar;
        _size--;

    }

}

template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    Nodo* y;
    Nodo* x = this->_raiz;

    while(x != y && x!=nullptr){
        if (x->valor == clave){
            y = x;
        } else if (x->valor < clave){
            y = x;
            x = x->der;
        } else if (x->valor > clave){
            y = x;
            x = x->izq;
        }
    }
    if(y -> der != nullptr){
    return minimoSubArbol(y -> der);
    }
    else{
        return y -> padre -> valor;
    }
}

template <class T>
const T& Conjunto<T>::minimoSubArbol(Nodo* z) const {
    while(z -> izq !=nullptr){
        z = z -> izq;
    }
    return z -> valor;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    return minimoSubArbol(this -> _raiz);
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo* z = this->_raiz;
    while(z -> der != nullptr){
        z = z -> der;
    }
    return z -> valor;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return _size;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

